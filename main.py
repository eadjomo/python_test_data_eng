import argparse

from ad_hoc_part.ad_hoc_part import get_journal_with_most_mentions
from etl.jobs.pipeline import run

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Data Pipeline')
    parser.add_argument('-d', '--drugs_file_path', help='drugs file path', required=True)
    parser.add_argument('-p', '--pubmed_csv_file_path', help='pubmed csv file path', required=True)
    parser.add_argument('-i', '--pubmed_json_file_path', help='pubmed json file path', required=True)
    parser.add_argument('-c', '--clinical_trials_file_path', help='clinical file path', required=True)
    parser.add_argument('-o', '--output', help='output path folder', required=True)
    parser.add_argument('-t', '--tmp', help='temporary working directory', required=True)
    parser.add_argument('-a', '--archive', help='archive folder ', required=True)
    args = parser.parse_args()
    # run the pipeline
    run(args)
    # run the ad hoc feature
    get_journal_with_most_mentions(f'{args.output}/result.json')
