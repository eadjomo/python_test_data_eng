## Compte rendu du test - ESSALE ADJOMO

## 0 - Architecture technique de  la Data Pipeline ou ETL

-----------

La Data Pipeline ou ETL est construit sur Apache Spark et  est constitué des modules suivants:


| Modules     | Description                                                                                                     |      input      |    output    |
|-------------|-----------------------------------------------------------------------------------------------------------------|:---------------:|:------------:|
| reader      | Module offrant les routines de lecture des données . Actuellement disponible un reader csv et un reader json    | Path du fichier |  Dataframe   |
| transformer | Module permettant l'implementation des transformations sql ou autres ( compatible Apache Spark 3.x)             |       3 Dataframe        |  Dataframe   |
| writer      | Module permettant de persister le resultat des transformations. Actuellement disponible un writer json en local |      Dataframe       | fichier json |
| job         | module decrivant une pipeline ( read ,transform,write)                                                          |      fffff      |     ffff     |
| main        | fonction de lancement de l'execution de la pipeline                                                             |      fffff      |     ffff     |

## 1- L'arborescence des fichiers


-----------
    ├── README.md
    ├── archive  : dossier contenant les fichiers archivés
    ├── data   : dossier contenant les fichiers d'input
    │ ├── clinical_trials.csv
    │ ├── drugs.csv
    │ ├── pubmed.csv
    │ └── pubmed.json
    ├── main.py   : programme principale
    ├── Makefile  
    ├── output      : dossier contenant le fichier resultat
    │ ├── result.json  : fichier contenant le resultat 
    │ 
    ├── requirements.txt
    ├── etl
    │ ├── __init__.py
    │ ├── helpers
    │ │ ├── __init__.py
    │ │ ├── helpers.py
    │ │ └── spark.py
    │ ├── job
    │ │ ├── __init__.py
    │ │ └── pipeline.py
    │ │── reader
    │ │   ├── __init__.py
    │ │   ├── readers.py
    │ │── transformer
    │ │    ├── __init__.py
    │ │    ├── transformer.py   
    │ │── writer
    │     ├── __init__.py
    │     ├── writer.py     
    │── doc
    │    ├──test_python_de.pdf  : sujet du test
    │     
    ├── setup.py
    ├── tests
    │     ├── __init__.py
    │     ├──test_ad_hoc_part.py
    │     ├──test_etl.py
    │     ├──test_reader.py
    │     │── test_reader_data
    │     │      ├── pubmed.csv
    │     │      └── pubmed.json
    │     │──test_data
    │           ├── clinical_trials.csv
    │           ├── drugs.csv
    │           ├── pubmed.csv
    │           └── pubmed.json
    └── tmp : dossier temporaire


## Comment executer le programme sur son poste de travail

Clone du projet

```bash
  git clone https://gitlab.com/eadjomo/python_test_data_eng.git
```

Allez dans le dossier du projet

```bash
  cd python_test_data_eng
```

Initialisation de l'environnement et installation des dependances

```bash
  make init
  make install
```

Executez les tests unitaires et verifier la qualité de code

```bash
  make lint
  make test
```

Executer le programme principale

```bash
  make run
```



## Comment executer depuis Gitlab-CI

Lancer une pipeline Gitlab en cliquant sur le bouton `Run pipeline` depuis la console Gitlab.

Et ensuite clique sur le bouton `Run pipeline`

![ALT](/docs/Capture d’écran 2023-01-18 à 03.01.54.png)


Pour executer le programe principale et la feature ad_hoc 

cliquer sur le bouton play de la tache run 

![ALT](/docs/Capture d’écran 2023-01-18 à 03.41.14.png)

Pour visualiser les résultats , ouvrir le stage run de la pipeline gitlab:
1.  Téléchargez le fichier artifact `output/result.json`
2. Dans les logs de la CI , est afficher le nom du journal qui mentionne le plus de médicaments différents .
   ![ALT](/docs/Capture d’écran 2023-01-18 à 03.47.42.png)

## Comment faire évoluer le code pour gérer de grosses volumétries
Voici mes propositions :
1. Ajouter un Writer pour stocker les données système de stockage adéquat comme GCP Bigquery
2. Dans le nouveau Writer , partitionner les données pour optimiser leur stockage et les analyses de celles - ci
3. Execution du programme sur une infrastructure managé type GCP Dataproc ou GCP Dataflow , Airflow 
4. Faire une refonte du connecteur Spark 
5. Revoir l'implémentation du DefaultTransformer pour l'orienter vers un usage de GCP BigQuery au travers de SQL 
et de son api pour bénéficier au maximum de la capacité et la puissance de calcul offerte.


## Réponses aux questions SQL
1.  SQL simple permettant de trouver le chiffre d’affaires (le montant total des ventes), 
jour par jour, du 1er janvier 2019 au 31 décembre 2019
```
    SELECT
        date,
        sum(prod_price * prod_qty) AS ventes
    FROM
        TRANSACTIONS
    where
        date between "01/01/2019"
        and "31/12/2019"
    GROUP BY
        date
    order by
        date;
```

2. La requête SQL permet de déterminer, par client et sur la période allant du 1er janvier 2019 au 31 décembre 2019 
, les ventes meuble et déco réalisées

```
    SELECT
        T.client_id,
        sum(
            IF(
                P.product_type = "MEUBLE",
                T.prod_price * T.prod_qty,
                0
            )
        ) AS ventes_meuble,
        sum(
            IF(
                P.product_type = "DECO",
                T.prod_price * T.prod_qty,
                0
            )
        ) AS ventes_deco
    FROM
        TRANSACTIONS AS T
        inner join PRODUCT_NOMENCLATURE AS P ON T.prod_id = P.product_id
    where
        date between "01/01/2019"
        and "31/12/2019"
    GROUP BY
        T.client_id;
```


## Authors

- [@eadjomo](https://gitlab.com/eadjomo/) Essale Adjomo



