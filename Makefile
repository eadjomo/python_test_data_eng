PY=python3
SHELL := /bin/bash
PYTHON3_OK := $(shell python3 --version 2> /dev/null)

.PHONY:
	init
	install
	test
	lint
	run

.DEFAULT: help
help:
	@echo "make init"
	@echo "       prepare development environment, use only once"
	@echo "make install"
	@echo "       install development requirements"
	@echo "make test"
	@echo "       run tests"
	@echo "make lint"
	@echo "       run pylint"
	@echo " make run"
	@echo "       main program is running "



init: check-python
	python3 -V
	python3 -m venv .env
	source .env/bin/activate
install:
	pip3 install -r requirements.txt
	python setup.py install
lint:
	pylint ./**/*.py --recursive=yes
test:
	pytest
run: set-env-var
	python main.py -d $(DRUGS_FILE_PATH) -p $(PUBMED_CSV_FILE_PATH) -i $(PUBMED_JSON_FILE_PATH) -c $(CLINICAL_TRIALS_FILE_PATH) -o $(OUTPUT_PATH) -t $(TMP_FOLDER_PATH) -a $(ARCHIVE_FOLDER_PATH)

check-python:
ifndef PYTHON3_OK
    	$(error package 'python3' not found)
endif

set-env-var:
export DRUGS_FILE_PATH=data/drugs.csv
export PUBMED_CSV_FILE_PATH=data/pubmed.csv
export PUBMED_JSON_FILE_PATH=data/pubmed.json
export CLINICAL_TRIALS_FILE_PATH=data/clinical_trials.csv
export TMP_FOLDER_PATH=tmp
export ARCHIVE_FOLDER_PATH=archive
export OUTPUT_PATH=output
