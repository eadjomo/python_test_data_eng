"""Module Transformer ."""
from __future__ import annotations

from abc import ABC, abstractmethod

from pyspark.sql import DataFrame
from pyspark.sql.functions import struct, col, lower, explode, array, collect_set


class Transformer(ABC):
    """
   The Transformer class declares the factory method  to return an
   object of a Transform class . The Transform's subclasses usually provide the
   implementation of this method.
   """

    @staticmethod
    def get_concrete_transformer(name: str) -> Transformer:
        """
        Get a concrete implementation of the transformer to use based on the type of the transformation
        """
        if name == 'default':
            return DefaultTransformer()
        raise ValueError(f'Unsupported transfom type: "{name}"')

    @staticmethod
    @abstractmethod
    def transform(drugs_df: DataFrame,
                  pubmed_csv_df: DataFrame,
                  pubmed_json_df: DataFrame,
                  clinical_trials_df: DataFrame) -> DataFrame:
        """
         Concrete Implementation of the transformation behavious
         :param drugs_df:
         :param pubmed_json_df:
         :param clinical_trials_df:
         :param pubmed_csv_df:
        """


class DefaultTransformer(Transformer):
    """
     The Concrete DefaultTransformer class
     """
    #pylint: disable=W0221
    def transform(self, drugs_df: DataFrame,
                  pubmed_csv_df: DataFrame,
                  pubmed_json_df: DataFrame,
                  clinical_trials_df: DataFrame) -> DataFrame:
        # initialization of the dataframe we needs
        clinical_trials = clinical_trials_df.withColumn("clinical_trials_json", struct(col("*"))) \
            .withColumn("clinical_trials_journal_json",
                        struct(col("clinical_trials_json.date"), col("clinical_trials_json.journal")))

        # get the list of fields from pubmed_df_csv dataframe
        fields = pubmed_csv_df.schema.names

        # pubmed_df_csv and pubmed_df_json have the same structure , so we can merge them
        pubmed_union_df = pubmed_csv_df.select(fields).union(pubmed_json_df.select(fields)) \
            .withColumn("pubmed_json", struct(col("*"))) \
            .withColumn("journal_json", struct(col("pubmed_json.date"), col("pubmed_json.journal")))

        # join the drugs dataframe with pubmed dataframe
        join_pubmed_drugs = pubmed_union_df.join(drugs_df) \
            .where(lower(col("title")).contains(lower(col("drug")))) \
            .select(list(drugs_df.schema.names + ['pubmed_json', 'journal_json']))

        # join the drugs dataframe with clinical_trials dataframe
        join_clinical_trials_drugs = clinical_trials.join(drugs_df) \
            .where(lower(col("scientific_title")).contains(lower(col("drug")))) \
            .select(list(drugs_df.schema.names + ['clinical_trials_json', 'clinical_trials_journal_json']))

        # merge the dataframes comes from the previous joins
        merge_pubmed_drugs_clinical_trials = join_pubmed_drugs \
            .join(join_clinical_trials_drugs, ["atccode", "drug"], "fullouter") \
            .withColumn("journal_drug", explode(array("journal_json", "clinical_trials_journal_json")))

        # group the merge dataframe by drug and return the result
        return merge_pubmed_drugs_clinical_trials.groupBy("atccode", "drug") \
            .agg(collect_set("pubmed_json").alias("medicals_publications_mentions"),
                 collect_set("clinical_trials_json").alias("clinical_trials_mentions"),
                 collect_set("journal_drug").alias("journal_mentions"))
