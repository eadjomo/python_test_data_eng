"""Module providingFunction printing python version."""
import logging
import os
import time
from pathlib import Path


def move_file(source: str, destination: str) -> str:
    """
    Move a source file to the destination path
    :rtype: object
    :param source: The path of the file to move
    :param destination: The folder path where moved the file
    :return: The new path of the file in the destination
    """
    try:
        logging.info(f'Move the file from: "{source}" to: "{destination}".')
        time_str = time.strftime("%Y%m%d-%H%M%S")
        file_name = Path(source).name
        new_path = f'{destination}/{time_str}_{file_name}'
        os.rename(source, new_path)
        return new_path
    except FileNotFoundError as exc:
        logging.error('File Not Found')
        raise FileNotFoundError(f"File  {source} Not Found") from exc
    except Exception as ex:
        logging.error('Some Other error')
        raise Exception(f"Failure moving file from  {source} to  {destination}. ( {str(ex)})") from ex


def archive_file(file_path: str, archive_path: str):
    """
    Move a file to the Archive folder
    :param file_path: The path of the file
    :param archive_path: The path where to archive the file
    """
    logging.info(f'Move  the file: "{file_path}" to archive folder: "{archive_path}".')
    move_file(file_path, archive_path)
    logging.info('File is archived')
