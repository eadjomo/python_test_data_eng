"""Module providingFunction printing python version."""
from pyspark.sql import SparkSession


def get_spark_session():
    """Get Spark Session."""
    return SparkSession.builder.master("local[*]").appName('My Python test data eng').getOrCreate()
