# -*- coding: utf-8 -*-
"""Module providingFunction printing python version."""
import logging

from pyspark.sql import DataFrame

from ..helpers.helpers import move_file, archive_file
from ..reader.readers import Reader
from ..transformer.transformer import Transformer
from ..writer.writer import Writer


def run(args):
    """Main function to start the  pipeline"""
    try:
        logging.getLogger().setLevel(logging.INFO)

        logging.info('The pipeline start.')
        # Move the input files to the temporary folder
        drugs_file_path: str = move_file(args.drugs_file_path, args.tmp)
        pubmed_csv_file_path: str = move_file(args.pubmed_csv_file_path, args.tmp)
        pubmed_json_file_path: str = move_file(args.pubmed_json_file_path, args.tmp)
        clinical_trials_file_path: str = move_file(args.clinical_trials_file_path, args.tmp)

        # Instantiate the Readers skeleton
        # pylint: disable=E1120
        csv_reader: Reader = Reader.get_concrete_reader(fmt='CSV')
        json_reader: Reader = Reader.get_concrete_reader(fmt='JSON')
        # Instantiate the transformer skeleton
        transformer: Transformer = Transformer.get_concrete_transformer(name='default')
        # Instantiate the writer skeleton
        writer: Writer = Writer.get_concrete_writer(name='default')

        # Start Process data
        dataframe: DataFrame = transformer.transform(csv_reader.read(drugs_file_path),
                                                     csv_reader.read(pubmed_csv_file_path),
                                                     json_reader.read(pubmed_json_file_path),
                                                     csv_reader.read(clinical_trials_file_path))
        # Write the result
        writer.write(dataframe, args.output)

        # archive input files
        archive_file(drugs_file_path, args.archive)
        archive_file(pubmed_csv_file_path, args.archive)
        archive_file(pubmed_json_file_path, args.archive)
        archive_file(clinical_trials_file_path, args.archive)

        logging.info('The pipeline succeeded  and the result is in the'
                     ' file named result.json on the working directory')

    except Exception as ex:
        logging.error('Data pipeline failed because of this error: %s', str(ex))
        raise ex
