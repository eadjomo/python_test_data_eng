"""Module Reader """
from __future__ import annotations
import json
from abc import ABC, abstractmethod

from jsoncomment import JsonComment
from pyspark.sql import DataFrame

from etl.helpers.spark import get_spark_session


class Reader(ABC):
    """
    The Reader class declares the factory method  to return an
    object of a Reader class : CSV or JSON. The Reader's subclasses usually provide the
    implementation of this method.
    """


    @staticmethod
    def get_concrete_reader(fmt: str) -> Reader:
        """
        Get a concrete implementation of the Reader to use based on the format of the input file
        :type fmt: object
        """
        if fmt == 'JSON':
            return JsonReader()
        if fmt == 'CSV':
            return CsvReader()
        raise ValueError(f'Unsupported file format: "{fmt}"')

    @staticmethod
    @abstractmethod
    def read(source: str) -> DataFrame:
        """
         Implementation of the read behavious
         :type source: object
         """


class CsvReader(Reader):
    """Concrete CSV reader"""
    #pylint: disable=W0221
    def read(self, source: str) -> DataFrame:
        # read the csv file into dataframe
        output_df = get_spark_session().read.options(header=True, inferSchema=True).csv(source)
        return output_df


class JsonReader(Reader):
    """Concrete Json reader"""
    #pylint: disable=W0221
    def read(self, source: str) -> DataFrame:
        # read the JSON file into dataframe

        with open(source, 'r', encoding='utf-8') as file:
            parser = JsonComment(json)
            json_list = parser.load(file)
        spark_session = get_spark_session()
        spark_context = spark_session.sparkContext
        output_df = get_spark_session().read.options(multiline=True).json(spark_context.parallelize(json_list))
        return output_df
