"""Module Writer ."""
from __future__ import annotations

import json
from abc import ABC, abstractmethod

from pyspark.sql import DataFrame


class Writer(ABC):
    """
  The Writer class declares the factory method  to return an
  object of a Writer class : CSV or JSON. The Writer's subclasses usually provide the
  implementation of this method.
  """

    @staticmethod
    def get_concrete_writer(name: str) -> Writer:
        """
        Get a concrete implementation of the Writer to use based on the type of the writer
        :rtype: object
        """
        if name == 'default':
            return JsonLocalWriter()
        raise ValueError(f'Unsupported writer type: "{name}"')
    @staticmethod
    @abstractmethod
    def write(datafame: DataFrame, destination: str):
        """
         Concrete Implementation of the writer behavious
         :param destination:
         :type datafame: object
         :param datafame:
        """


class JsonLocalWriter(Writer):
    """
    Get a concrete implementation of the Writer to create save the result as a json file on local file system
    """
    #pylint: disable=W0221
    def write(self, datafame: DataFrame, destination: str):
        """
         Concrete Implementation of the transformation behavious
         :param destination:
         :rtype: object
         :param datafame:
        """
        try:
            df_list_of_jsons = datafame.toJSON().collect()
            df_list_of_dicts = [json.loads(x) for x in df_list_of_jsons]
            with open(f'{destination}/result.json', 'w', encoding="utf-8") as json_file:
                json_file.write(json.dumps(df_list_of_dicts, ensure_ascii=False, indent=4))
                json_file.close()
        except Exception as ex:
            raise ex
