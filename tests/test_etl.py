# -*- coding: utf-8 -*-
"""
Tests Case
"""
import argparse
import os
import unittest

from pyspark.sql.functions import col, explode

from etl.jobs.pipeline import run
from etl.reader.readers import Reader


class PipelineTestSuite(unittest.TestCase):
    """ Run Pipeline Test suite"""


    def test_etl_input(self):
        """
        Test etl
        :return:
        """
        parser = argparse.ArgumentParser(description='Data Pipeline')
        parser.add_argument('-d', '--drugs_file_path', help='drugs file path', required=True,
                            default='tests/tests_data/drugs.csv')
        parser.add_argument('-p', '--pubmed_csv_file_path', help='pubmed csv file path', required=True,
                            default='tests/tests_data/pubmed.csv')
        parser.add_argument('-i', '--pubmed_json_file_path', help='pubmed json file path', required=True,
                            default='test/tests_data/pubmed.json')
        parser.add_argument('-c', '--clinical_trials_file_path', help='clinical file path', required=True,
                            default='test/tests_data/clinical_trials.csv')
        parser.add_argument('-o', '--output', help='output path folder', required=True, default='tests')
        parser.add_argument('-t', '--tmp', help='temporary working directory', required=True,
                            default='tests/tests_data/tmp')
        parser.add_argument('-a', '--archive', help='archive working directory', required=True,
                            default='tests/tests_data/archive')
        args = parser.parse_args(['-d', 'tests/tests_data/drugs.csv',
                                  '-p', 'tests/tests_data/pubmed.csv',
                                  '-i', 'tests/tests_data/pubmed.json',
                                  '-c', 'tests/tests_data/clinical_trials.csv',
                                  '-o', 'tests',
                                  '-t', 'tests/tests_data/tmp',
                                  '-a', 'tests/tests_data/archive'])
        os.makedirs('tests/tests_data/archive', exist_ok=True)
        os.makedirs('tests/tests_data/tmp', exist_ok=True)
        run(args)
        json_reader = Reader.get_concrete_reader(fmt="JSON")
        df_df = json_reader.read("tests/result.json")
        df_df.printSchema()
        self.assertEqual(df_df.count(), 7)
        self.assertEqual(df_df.select(explode(col("medicals_publications_mentions"))).count(), 14)
        self.assertEqual(df_df.where("atccode='A01AD'").
                         select(explode(col("medicals_publications_mentions"))).count(), 2)
        self.assertEqual(df_df.where("atccode='A04AD'")
                         .select(explode(col("medicals_publications_mentions"))).count(), 3)

        # merg.where("jour_drug1.journal is not null")
        # .groupBy("jour_drug1.journal").agg(countDistinct('atccode')
        # .alias("atccode_dt")).sort(desc("atccode_dt")).show(truncate=False)

        # self.assertTrue(True)


if __name__ == '__main__':
    unittest.main()
