# -*- coding: utf-8 -*-
"""
Tests Case
"""
import unittest

from etl.reader.readers import Reader, CsvReader, JsonReader


class ReaderTestSuite(unittest.TestCase):
    """ Read Test cases"""

    def test_get_concrete_reader(self):
        """ Test Case to check the concrete reader initialization"""
        self.assertTrue(isinstance(Reader.get_concrete_reader(fmt="CSV"), CsvReader))
        self.assertTrue(isinstance(Reader.get_concrete_reader(fmt="JSON"), JsonReader))

        with self.assertRaises(ValueError) as context:
            Reader.get_concrete_reader(fmt="XML")
        self.assertTrue('Unsupported file format: "XML"' in str(context.exception))

    def test_read_csv_input(self):
        """ Read csv Test case"""
        reader = Reader.get_concrete_reader(fmt="CSV")
        result = reader.read('tests/test_reader_data/pubmed.csv')
        self.assertTrue(result.count(), 7)

    def test_read_json_input(self):
        """ read json test case"""
        json_reader = Reader.get_concrete_reader(fmt="JSON")
        result = json_reader.read('tests/test_reader_data/pubmed.json')
        self.assertTrue(result.count(), 6)


if __name__ == '__main__':
    unittest.main()
