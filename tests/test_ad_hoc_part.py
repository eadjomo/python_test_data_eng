# -*- coding: utf-8 -*-
"""Module providingFunction printing python version."""
import unittest

from ad_hoc_part.ad_hoc_part import get_journal_with_most_mentions


class SecondPartTestSuite(unittest.TestCase):
    """Basic test cases."""

    def test_get_journal_with_most_mentions(self):
        """get_journal_with_most_mentions"""

        self.assertTrue(get_journal_with_most_mentions('tests/result.json'), "journal of emergency nursing")
        assert True


if __name__ == '__main__':
    unittest.main()
