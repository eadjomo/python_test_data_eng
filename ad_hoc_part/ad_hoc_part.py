""" Get Journal with most different drugs mentions"""
import logging
import re

from pyspark.sql.functions import explode, countDistinct, asc, \
    monotonically_increasing_id

from etl.reader.readers import Reader


def get_journal_with_most_mentions(file_path: str) -> str:
    """ read json test case"""
    logging.getLogger().setLevel(logging.INFO)

    # init the json reader stub
    json_reader = Reader.get_concrete_reader(fmt="JSON")
    logging.info(f'read the json file:{file_path}')
    result_df = json_reader.read(file_path)

    # Select the drug , the date and the journal name
    # convert the journal name to lowercase and clean up all special chars
    # DESC Sort the result and get the head of the dataframe

    journal_with_most_mentions = result_df.select("atccode", "journal_mentions") \
        .select("atccode", explode("journal_mentions").alias("journal_mentions")) \
        .selectExpr("atccode", "journal_mentions.date as date", "lower(journal_mentions.journal) as journal") \
        .rdd.map(lambda x:
                 (x.atccode, x.date, re.sub(r'([\\w\\d]*)', r'', x.journal))
                 ).toDF(["atccode", "date", "journal"]) \
        .selectExpr("atccode", "date", "replace(journal,'xc3x28','') as journal") \
        .groupBy("journal").agg(countDistinct('atccode').alias("nb_atccode")) \
        .sort(asc("nb_atccode")) \
        .orderBy(monotonically_increasing_id().desc()).head()[0]

    logging.info(f'the journal with most mentions is {journal_with_most_mentions}')
    return journal_with_most_mentions
